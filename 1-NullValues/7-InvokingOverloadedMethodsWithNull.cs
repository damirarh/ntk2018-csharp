﻿using NUnit.Framework;

namespace _1_NullValues
{
    [TestFixture]
    public class _7_InvokingOverloadedMethodsWithNull
    {
        [Test]
        public void OverloadWithObjectParameterCalled()
        {
            var instance = new SampleClass();
            var result = instance.OverloadedMethod(null);

            Assert.AreEqual("object", result);
        }

        [Test]
        public void OverloadWithStringParameterCalled()
        {
            var instance = new SampleClass();
            var result = instance.OverloadedMethod(null);

            Assert.AreEqual("string", result);
        }
    }
}
