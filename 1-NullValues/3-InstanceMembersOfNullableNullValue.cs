﻿using NUnit.Framework;
using System;

namespace _1_NullValues
{
    [TestFixture]
    public class _3_InstanceMembersOfNullableNullValue
    {
        [Test]
        public void CannotInvokeInstanceMethodsOfNull()
        {
            int? nullableInt = null;

            Assert.Throws<NullReferenceException>(() => { var foo = nullableInt.HasValue; });
        }

        [Test]
        public void CanInvokeInstanceMethodsOfNull()
        {
            int? nullableInt = null;

            Assert.DoesNotThrow(() => { var foo = nullableInt.HasValue; });
        }
    }
}
