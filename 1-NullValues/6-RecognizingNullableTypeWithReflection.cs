﻿using NUnit.Framework;
using System;

namespace _1_NullValues
{
    [TestFixture]
    public class _6_RecognizingNullableTypeWithReflection
    {
        [Test]
        public void NullableAndNonNullableTypesAreNotOfSameType()
        {
            Type nullableType = typeof(int?);
            Type nonNullableType = typeof(int);

            Assert.AreNotEqual(nonNullableType, nullableType);
        }

        [Test]
        public void NullableAndNonNullableTypesAreOfSameType()
        {
            Type nullableType = typeof(int?);
            Type nonNullableType = typeof(int);

            Assert.AreEqual(nonNullableType, nullableType);
        }
    }
}
