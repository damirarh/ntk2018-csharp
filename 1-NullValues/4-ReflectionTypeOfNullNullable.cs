﻿using NUnit.Framework;
using System;

namespace _1_NullValues
{
    [TestFixture]
    public class _4_ReflectionTypeOfNullNullable
    {
        [Test]
        public void NullableValueIsOfNullableType()
        {
            int? nullableInt = null;
            Type type = nullableInt.GetType();

            Assert.AreEqual(typeof(Nullable<int>), type);
        }

        [Test]
        public void NullableValueIsNotOfNullableType()
        {
            int? nullableInt = null;
            Type type = nullableInt.GetType();

            Assert.AreNotEqual(typeof(Nullable<int>), type);
        }
    }
}
