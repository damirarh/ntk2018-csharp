﻿using NUnit.Framework;

namespace _1_NullValues
{
    [TestFixture]
    public class _5_RecognizingNullableTypeAtRuntime
    {
        [Test]
        public void NullableAndNonNullableTypesAreNotOfSameType()
        {
            int? nullable = 42;
            int nonNullable = 42;

            Assert.AreNotEqual(nonNullable.GetType(), nullable.GetType());
        }

        [Test]
        public void NullableAndNonNullableTypesAreOfSameType()
        {
            int? nullable = 42;
            int nonNullable = 42;

            Assert.AreEqual(nonNullable.GetType(), nullable.GetType());
        }
    }
}
