﻿using NUnit.Framework;
using System;

namespace _1_NullValues
{
    [TestFixture]
    public class _2_ReflectionTypeOfNullString
    {
        [Test]
        public void NullStringIsOfTypeNull()
        {
            string nullString = null;
            Type type = nullString.GetType();

            Assert.AreEqual(typeof(string), type);
        }

        [Test]
        public void NullStringIsNotOfTypeNull()
        {
            string nullString = null;
            Type type = nullString.GetType();

            Assert.AreNotEqual(typeof(string), type);
        }
    }
}
