﻿using NUnit.Framework;

namespace _1_NullValues
{
    [TestFixture]
    public class _1_RuntimeTypeOfNullString
    {
        [Test]
        public void NullStringIsOfTypeString()
        {
            string nullString = null;

            Assert.IsTrue(nullString is string);
        }

        [Test]
        public void NullStringIsNotOfTypeString()
        {
            string nullString = null;

            Assert.IsFalse(nullString is string);
        }
    }
}
