﻿namespace _1_NullValues
{
    class SampleClass
    {
        public string OverloadedMethod(object arg)
        {
            return "object";
        }

        public string OverloadedMethod(string arg)
        {
            return "string";
        }
    }
}
