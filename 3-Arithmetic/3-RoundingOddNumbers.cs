﻿using NUnit.Framework;
using System;

namespace _3_Arithmetic
{
    [TestFixture]
    public class _3_RoundingOddNumbers
    {
        [Test]
        public void RoundsOddUp()
        {
            var roundedValue = Math.Round(1.5);

            Assert.AreEqual(2, roundedValue);
        }

        [Test]
        public void RoundsOddDown()
        {
            var roundedValue = Math.Round(1.5);

            Assert.AreEqual(1, roundedValue);
        }
    }
}
