﻿using NUnit.Framework;

namespace _3_Arithmetic
{
    [TestFixture]
    public class _1_ShiftBy1Wrapping
    {
        [Test]
        public void ShiftBy1WrapsBits()
        {
            uint original = 0b1;
            uint shifted = original >> 1;

            Assert.AreEqual(0b1 << 31, shifted);
        }

        [Test]
        public void ShiftBy1DoesntWrapBits()
        {
            uint original = 0b1;
            uint shifted = original >> 1;

            Assert.AreEqual(0, shifted);
        }
    }
}
