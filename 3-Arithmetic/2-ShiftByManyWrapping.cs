﻿using NUnit.Framework;

namespace _3_Arithmetic
{
    [TestFixture]
    public class _2_ShiftByManyWrapping
    {
        [Test]
        public void ShiftBy32WrapsBits()
        {
            uint original = 0b1;
            uint shifted = original >> 32;

            Assert.AreEqual(0b1, shifted);
        }

        [Test]
        public void ShiftBy32DoesntWrapBits()
        {
            uint original = 0b1;
            uint shifted = original >> 32;

            Assert.AreEqual(0, shifted);
        }
    }
}
