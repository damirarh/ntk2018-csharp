﻿using NUnit.Framework;
using System;

namespace _3_Arithmetic
{
    [TestFixture]
    public class _4_RoundingEvenNumbers
    {
        [Test]
        public void RoundsEvenUp()
        {
            var roundedValue = Math.Round(2.5);

            Assert.AreEqual(3, roundedValue);
        }

        [Test]
        public void RoundsEvenDown()
        {
            var roundedValue = Math.Round(2.5);

            Assert.AreEqual(2, roundedValue);
        }
    }
}
