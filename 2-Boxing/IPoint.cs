﻿namespace _2_Boxing
{
    interface IPoint
    {
        int X { get; set; }
        int Y { get; set; }
    }
}
