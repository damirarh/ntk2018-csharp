﻿using NUnit.Framework;

namespace _2_Boxing
{
    class _4_ModifyingStructsViaInterfaces
    {
        [Test]
        public void ModifiesOriginalInterface()
        {
            Point pointStruct = new Point(1, 1);
            IPoint pointInterface = pointStruct;
            pointInterface.X = 2;

            Assert.AreEqual(2, pointStruct.X);
        }

        [Test]
        public void DoesntModifyOriginalInterface()
        {
            Point pointStruct = new Point(1, 1);
            IPoint pointInterface = pointStruct;
            pointInterface.X = 2;

            Assert.AreEqual(1, pointStruct.X);
        }
    }
}
