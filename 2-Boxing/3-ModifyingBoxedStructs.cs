﻿using NUnit.Framework;

namespace _2_Boxing
{
    [TestFixture]
    public class _3_ModifyingBoxedStructs
    {
        [Test]
        public void ModifiesOriginalStruct()
        {
            object o = new Point(1, 1);
            Point p = (Point)o;
            p.X = 2;

            Assert.AreEqual(2, ((Point)o).X);
        }

        [Test]
        public void DoesntModifyOriginalStruct()
        {
            object o = new Point(1, 1);
            Point p = (Point)o;
            p.X = 2;

            Assert.AreEqual(1, ((Point)o).X);
        }
    }
}
