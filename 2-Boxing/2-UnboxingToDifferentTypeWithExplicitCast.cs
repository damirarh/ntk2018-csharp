﻿using NUnit.Framework;
using System;

namespace _2_Boxing
{
    [TestFixture]
    public class _2_UnboxingToDifferentTypeWithExplicitCast
    {
        [Test]
        public void CanUnboxToDifferentTypeWithCast()
        {
            object o = 42;

            Assert.DoesNotThrow(() => { long unbox = (int)o; });
        }

        [Test]
        public void CannotUnboxToDifferentTypeWithCast()
        {
            object o = 42;

            Assert.Throws<InvalidCastException>(() => { long unbox = (int)o; });
        }
    }
}
