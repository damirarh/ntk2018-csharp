﻿using NUnit.Framework;
using System;

namespace _2_Boxing
{
    [TestFixture]
    public class _1_UnboxingToDifferentType
    {
        [Test]
        public void CanUnboxToDifferentType()
        {
            object o = 42;

            Assert.DoesNotThrow(() => { long unbox = (long)o; });
        }

        [Test]
        public void CannotUnboxToDifferentType()
        {
            object o = 42;

            Assert.Throws<InvalidCastException>(() => { long unbox = (long)o; });
        }
    }
}
