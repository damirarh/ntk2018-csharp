﻿namespace _2_Boxing
{
    struct Point : IPoint
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }
    }
}
