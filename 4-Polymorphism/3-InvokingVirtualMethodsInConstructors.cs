﻿using NUnit.Framework;

namespace _4_Polymorphism
{
    [TestFixture]
    public class _3_InvokingVirtualMethodsInConstructors
    {
        [Test]
        public void WillInvokeBaseClassMethod()
        {
            var instance = new DerivedClass();

            Assert.AreEqual(2, instance.VirtualMethodResult);
        }

        [Test]
        public void WillInvokeDerivedClassMethod()
        {
            var instance = new DerivedClass();

            Assert.AreEqual(1, instance.VirtualMethodResult);
        }
    }
}
