﻿using System;

namespace _4_Polymorphism
{
    static class Config
    {
        public static bool ThrowException { get; set; }
    }

    class FailingClass
    {
        static FailingClass()
        {
            if (Config.ThrowException)
            {
                throw new InvalidOperationException();
            }
        }
    }
}
