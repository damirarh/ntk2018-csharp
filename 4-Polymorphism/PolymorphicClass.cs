﻿namespace _4_Polymorphism
{
    interface IPolymorphicInterface
    {
        string Method();
    }

    class PolymorphicBase
    {
        public virtual string Method()
        {
            return "PolymorphicBase";
        }
    }

    class PolymorphicClass : PolymorphicBase, IPolymorphicInterface
    {
        public new string Method()
        {
            return "PolymorphicClass";
        }

        string IPolymorphicInterface.Method()
        {
            return "IPolymorphicInterface";
        }
    }
}
