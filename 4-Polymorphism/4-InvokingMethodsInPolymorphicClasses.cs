﻿using NUnit.Framework;

namespace _4_Polymorphism
{
    [TestFixture]
    public class _4_InvokingMethodsInPolymorphicClasses
    {
        [Test]
        public void InvokeDerivedClassMethod()
        {
            var instance = new PolymorphicClass();
            var result = instance.Method();

            Assert.AreEqual("PolymorphicClass", result);
        }

        [Test]
        public void InvokeBaseClassMethod()
        {
            var instance = new PolymorphicClass();
            var result = ((PolymorphicBase)instance).Method();

            Assert.AreEqual("PolymorphicBase", result);
        }

        [Test]
        public void InvokeInterfaceMethod()
        {
            var instance = new PolymorphicClass();
            var result = ((IPolymorphicInterface)instance).Method();

            Assert.AreEqual("IPolymorphicInterface", result);
        }
    }
}
