﻿using NUnit.Framework;
using System;

namespace _4_Polymorphism
{
    [TestFixture]
    public class _2_FixingExceptionsInStaticConstructor
    {
        [Test]
        public void ConstructorThrowsException()
        {
            Config.ThrowException = true;
            try
            {
                new FailingClass();
            }
            catch { }
            Config.ThrowException = false;

            Assert.Throws<TypeInitializationException>(() => new FailingClass());
        }

        [Test]
        public void ConstructorDoesntThrowException()
        {
            Config.ThrowException = true;
            try
            {
                new FailingClass();
            }
            catch { }
            Config.ThrowException = false;

            Assert.DoesNotThrow(() => new FailingClass());
        }
    }
}
