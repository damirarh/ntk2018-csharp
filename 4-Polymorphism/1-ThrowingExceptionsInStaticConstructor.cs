﻿using NUnit.Framework;
using System;

namespace _4_Polymorphism
{
    [TestFixture]
    public class _1_ThrowingExceptionsInStaticConstructor
    {
        [Test]
        public void ConstructorThrowsException()
        {
            Config.ThrowException = true;

            Assert.Throws<TypeInitializationException>(() => new FailingClass());
        }

        [Test]
        public void ConstructorDoesntThrowException()
        {
            Config.ThrowException = true;

            Assert.DoesNotThrow(() => new FailingClass());
        }
    }
}
