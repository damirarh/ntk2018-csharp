﻿namespace _4_Polymorphism
{
    class BaseClass
    {
        public int VirtualMethodResult { get; private set; }

        public BaseClass()
        {
            VirtualMethodResult = VirtualMethod(2);
        }

        public virtual int VirtualMethod(int dividend)
        {
            return dividend / 1;
        }
    }

    class DerivedClass : BaseClass
    {
        int divisor;
        public DerivedClass()
        {
            divisor = 2;
        }

        public override int VirtualMethod(int dividend)
        {
            return base.VirtualMethod(dividend / divisor);
        }
    }
}
